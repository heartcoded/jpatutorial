
In cazul in care voiam sa apelez metoda de creare FidelityCard cu urmatorul requestbody crapa (nu stia de propietatea Client): 
{
  "points": 1,
  "client": {
  	"clientId":3
  }
}
Solutia a fost sa folosesc adnotarile :
a) @JsonManagedReference  - pusa pe campul Client(parintele) din FidelityCard. Jsonul va printa tot de la entitatea copil pana la entitatea parinte(ca dependinta) 
b) @JsonBackReference - pusa pe campul FidelityCard(copilul) din Client. Jsonul va ignora serializarea acestei dependinte de ex cand apeleam getClients. 

Diferenta intre @JsonIgnore si @JsonBackReference:
a) la SERILIZARE se comporta la fel (afisarea in json): in ambele cazuri , campul adnotat cu una dintre cele nu este afisat (nu este serializat) 
b) diferenta este la DESERIALIZARE: 
	pentru JsonIgnore, daca vin cu requestul:
		{
		  "points": 1,
		  "client": {
		  	"clientId":3
		  }
		}
		imi va pune pe NULL la client .
	pentru JsonBackReference pune corect , instantiaza obiectul si ii da id-ul 3.

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"} - daca folosim fetch type Lazy, The problem is that entities are loaded lazily and serialization happens BEFORE they get loaded fully.

//mappedBy de obicei in clasa parinte -> valoarea mappedBy = numele field-ului din clasa copil
//cascadeType = ALL; tot ce se salveaza/sterge etc in tabela parinte sa se evidentieze si in copil