package com.springboot.learning.jpatutorial.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.springboot.learning.jpatutorial.model.Client;
import com.springboot.learning.jpatutorial.service.impl.ClientServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description = "This controller exposes clients ws")
public class ClientController {

	@Autowired
	private ClientServiceImpl clientService;

	@GetMapping("/clients")
	@ApiOperation(value = "Retrieve all clients")
	public List<Client> retrieveClients() {
		return clientService.getClients();
	}

	// TODO: De tratat exceptii custom
	@GetMapping("/clients/{id}")
	@ApiOperation(value = "Retrive client with requested id")
	public Client retrieveClientById(@PathVariable Long id) {
		return clientService.getClientById(id);
	}

	// TODO: De tratat exceptii custom
	@DeleteMapping("/clients/{id}")
	@ApiOperation(value = "Delete client with requested id. If client owns a card, it will be deleted too")
	public void deleteClientById(@PathVariable Long id) {
		clientService.deleteClient(id);
	}

	// TODO: De tratat exceptii custom
	@PostMapping("/clients")
	@ApiOperation(value = "Create a new client with or without a fidelity card")
	public ResponseEntity<Object> createClient(@RequestBody Client client) {
		Client savedClient = clientService.insertClient(client);

		// in response's header give the location where the new resource is created
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedClient.getClientId()).toUri();

		return ResponseEntity.created(location).build();

	}

	@PutMapping("/clients/{id}")
	@ApiOperation(value = "Update client's fields.Can't update FidelityCard object")
	public ResponseEntity<Object> updateClient(@RequestBody Client client, @PathVariable long id) {

		Client existingClient = clientService.getClientById(id);

		if (existingClient == null) {
			return ResponseEntity.notFound().build();
		}
		if (client.getFidelityCard() != null) {
			// nu il lasam sa updateze de aici cardul
			return null;
		}

		client.setClientId(id);
		clientService.updateClient(client);
		return ResponseEntity.noContent().build();
	}

}
