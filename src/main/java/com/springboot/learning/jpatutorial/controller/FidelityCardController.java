package com.springboot.learning.jpatutorial.controller;

import java.net.URI;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.springboot.learning.jpatutorial.model.FidelityCard;
import com.springboot.learning.jpatutorial.service.FidelityCardService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description="This controller exposes fidelity-cards ws")
public class FidelityCardController {

	final static Logger LOGGER = Logger.getLogger(FidelityCardController.class);
	@Autowired
	private FidelityCardService fidelityCardService;

	@GetMapping("/fidelity-cards")
	@ApiOperation(value="Retrieve all cards")
	public List<FidelityCard> retrieveFidelityPoints() {
		return fidelityCardService.getFidelityCards();
	}

	@GetMapping("/fidelity-cards/{id}")
	@ApiOperation(value="Retrive card with requested id")
	public FidelityCard retrieveFidelityPoints(@PathVariable(value = "id") Long id) {
		return fidelityCardService.getFidelityCardById(id);
	}

	//TODO: de tratat exceptii custom
	@DeleteMapping("/fidelity-cards/{id}")
	@ApiOperation(value="Delete card with requested id.Client is not removed")
	public void deleteFidelityPoints(@PathVariable(value = "id") Long id) {
		fidelityCardService.deleteFidelityCard(id);
	}

	// TODO: De tratat exceptii custom
	@PostMapping("/fidelity-cards")
	@ApiOperation(value="Create a new card. The client's id must be specified.Client's not allowed to own more than one card")
	public ResponseEntity<Object> createFidelityCard(@RequestBody FidelityCard fidelityCard) {
		
		
		LOGGER.debug("##Enter in createFidelityCard controller's handler");
		
		FidelityCard savedCard = fidelityCardService.createFidelityCard(fidelityCard);

		if (savedCard != null) {
		 // in response's header give the location where the new resource is created 
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(savedCard.getFdlId()).toUri();
			return ResponseEntity.created(location).build();
		}
		return null;

	}
	
	//TODO: update fidelityCard
	

}
