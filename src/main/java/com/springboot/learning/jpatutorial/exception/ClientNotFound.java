package com.springboot.learning.jpatutorial.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClientNotFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2634896350876839346L;

	public ClientNotFound(String message) {
		super(message);
	}
}
