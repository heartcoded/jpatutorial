package com.springboot.learning.jpatutorial.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FidelityCardNotFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2542659787253313529L;

	public FidelityCardNotFound(String message) {
		super(message);
	}
}
