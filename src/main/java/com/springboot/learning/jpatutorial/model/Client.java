package com.springboot.learning.jpatutorial.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="JPA_CLIENTS")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@DynamicUpdate
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="clientSeq") 
    @SequenceGenerator(name="clientSeq",sequenceName="JPA_CLIENT_SEQ", allocationSize=1) 
	@Column(updatable = false)
	@ApiModelProperty(notes = "The database generated client ID", required = true)
	private Long clientId;
	
	@Column(name="FIRST_NAME")
	@ApiModelProperty(required = true)
	private String firstName;
	
	@Column(name="LAST_NAME")
	@ApiModelProperty(required = true)
	private String lastName;
	
	@ApiModelProperty(required = true)
	private String phone;
	
	
	@OneToOne(mappedBy="client", cascade=CascadeType.ALL,fetch=FetchType.LAZY)

    @JsonBackReference
	private FidelityCard fidelityCard;
}
