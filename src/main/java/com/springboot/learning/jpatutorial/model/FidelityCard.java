package com.springboot.learning.jpatutorial.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="JPA_FIDELITY_CARDS")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@DynamicUpdate
public class FidelityCard {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="cardSeq") 
    @SequenceGenerator(name="cardSeq",sequenceName="JPA_FDL_POINTS_SEQ", allocationSize=1) 
	@Column(updatable = false)
	@ApiModelProperty(notes = "The database generated product ID", required = true)
	private Long fdlId;
	
	@Column(name="FDL_POINTS")
	@ApiModelProperty(required = true)
	private int points;
	
	@OneToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH},fetch=FetchType.LAZY)
	@JoinColumn(name = "CLIENT_ID")
	@JsonManagedReference
	private Client client;
	

}
