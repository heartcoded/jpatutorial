package com.springboot.learning.jpatutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.learning.jpatutorial.model.Client;
import com.springboot.learning.jpatutorial.model.FidelityCard;

@Repository
public interface FidelityCardRepository extends JpaRepository<FidelityCard, Long> {

	public FidelityCard findByClient(Client client);
}
