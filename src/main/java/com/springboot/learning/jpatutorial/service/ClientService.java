package com.springboot.learning.jpatutorial.service;

import java.util.List;

import com.springboot.learning.jpatutorial.model.Client;

public interface ClientService {

	public List<Client> getClients();
	public Client insertClient(Client client);
	public void deleteClient(Long id);
	public Client getClientById(Long id);
	
	/**
	 * used in case of editing client's fields 
	 * @param client
	 * @return client
	 */
	public void updateClient(Client client);
	
}
