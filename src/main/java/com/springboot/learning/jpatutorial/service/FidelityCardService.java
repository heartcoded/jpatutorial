package com.springboot.learning.jpatutorial.service;

import java.util.List;

import com.springboot.learning.jpatutorial.model.Client;
import com.springboot.learning.jpatutorial.model.FidelityCard;

public interface FidelityCardService {

	public List<FidelityCard> getFidelityCards();
	public FidelityCard getFidelityCardById(Long id);
	public FidelityCard getFidelityCardByClient(Client client);
	public void deleteFidelityCard(Long id);
	public FidelityCard createFidelityCard(FidelityCard fidelityCard);
}

/*
 * De testat ce se intampla daca am CascadeType.ALL La client si dau sa salvez un client (fara card) -> merge
 */
