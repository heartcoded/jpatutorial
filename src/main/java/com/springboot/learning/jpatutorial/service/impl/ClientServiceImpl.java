package com.springboot.learning.jpatutorial.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.learning.jpatutorial.exception.ClientNotFound;
import com.springboot.learning.jpatutorial.model.Client;
import com.springboot.learning.jpatutorial.model.FidelityCard;
import com.springboot.learning.jpatutorial.repository.ClientRepository;
import com.springboot.learning.jpatutorial.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	final static Logger LOGGER = Logger.getLogger(ClientServiceImpl.class);
	@Autowired
	private ClientRepository clientRepository;

	@Override
	public List<Client> getClients() {

		return clientRepository.findAll();
	}

	@Override
	public Client insertClient(Client client) {
		if (client.getFidelityCard() != null) {
			FidelityCard fc = client.getFidelityCard();
			fc.setClient(client);
		}
		return clientRepository.save(client);
	}

	@Override
	public void deleteClient(Long id) {
		Client client = getClientById(id);
		if (client == null) {
			throw new ClientNotFound("id = " + id + " not found");
		}
		if (client.getFidelityCard() != null) {
			// biderectional : we also need to close client's card
			FidelityCard fc = client.getFidelityCard();
			fc.setClient(null);
		}
		clientRepository.deleteById(id);

	}

	@Override
	public Client getClientById(Long id) {
		Optional<Client> client = clientRepository.findById(id);
		if (!client.isPresent()) {
			throw new ClientNotFound("id = "+id+ " not found");
		}
		return client.get();

	}

	@Override
	public void updateClient(Client client) {
		clientRepository.save(client);
	}

}
