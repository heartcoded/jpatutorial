package com.springboot.learning.jpatutorial.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.learning.jpatutorial.exception.FidelityCardNotFound;
import com.springboot.learning.jpatutorial.model.Client;
import com.springboot.learning.jpatutorial.model.FidelityCard;
import com.springboot.learning.jpatutorial.repository.FidelityCardRepository;
import com.springboot.learning.jpatutorial.service.FidelityCardService;

@Service
public class FidelityCardServiceImpl implements FidelityCardService {

	final static Logger LOGGER = Logger.getLogger(FidelityCardServiceImpl.class);
	@Autowired
	private FidelityCardRepository fidelityCardRepository;

	@Override
	public List<FidelityCard> getFidelityCards() {
		return fidelityCardRepository.findAll();
	}

	@Override
	public FidelityCard getFidelityCardById(Long id) {
		Optional<FidelityCard> fidelityCard = fidelityCardRepository.findById(id);
		if (!fidelityCard.isPresent()) {
			throw new FidelityCardNotFound("id = "+id+ " not found");
		}
		return fidelityCard.get();
	}

	@Override
	public void deleteFidelityCard(Long id) {
		fidelityCardRepository.deleteById(id);

	}

	// TODO: ce se intampla daca bagam null ?
	@Override
	public FidelityCard getFidelityCardByClient(Client client) {

		return fidelityCardRepository.findByClient(client);
	}

	// TODO: testing this method
	@Override
	public FidelityCard createFidelityCard(FidelityCard fidelityCard) {
		LOGGER.debug("FidelityCard info: " + fidelityCard.getPoints() + ", id client "
				+ fidelityCard.getClient().getClientId());
		// TODO: check if client already has one card
		FidelityCard existingCard = fidelityCardRepository.findByClient(fidelityCard.getClient());

		if (existingCard != null) {
			LOGGER.debug("Existing card? " + existingCard.getFdlId());
			// TODO: return exception because client already has one card
			return null;
		}
		return fidelityCardRepository.save(fidelityCard);
	}

}
